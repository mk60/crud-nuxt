import firebase from 'firebase/app';

var firebaseConfig = {
    apiKey: "AIzaSyC7tqUR4Ci5tCPtYN7arlm9XxV8pwyAbTg",
    authDomain: "crud-189d2.firebaseapp.com",
    databaseURL: "https://crud-189d2-default-rtdb.firebaseio.com",
    projectId: "crud-189d2",
    storageBucket: "crud-189d2.appspot.com",
    messagingSenderId: "567769983112",
    appId: "1:567769983112:web:2842382bd65755cadd173d"
  };

  let app = null;
  if (!firebase.apps.length) {
    app = firebase.initializeApp(firebaseConfig);
  }
  // Initialize Firebase
